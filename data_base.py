import tkinter
from tkinter import messagebox
import sys
from documental import docdb
from relative import db_creator
from graph import red
import graphviz
from PIL import Image, ImageTk

pkg_name = None


def get_text():
    global pkg_name
    got = entry.get()
    if got:
        pkg_name = got

def converter(pkg):
    dot = graphviz.Digraph('package_deps') 
    for obj in pkg['deps']:
        dot.edge(obj, pkg['name'])
    for obj in pkg['provs']:
        dot.edge(pkg['name'], obj)
    dot = dot.unflatten(stagger = int(max(len(pkg['deps']), len(pkg['provs'])) / 3))
    return dot.render('TMP_graph', format = 'png')

def viewer(result):
    if not result['deps'] and not result['provs']:
        text = 'No such package'
        messagebox.showerror(message = text)
        return

    picture = converter(result)

    image = Image.open(picture)
    width = min(500, image.width)
    height = min(440, image.height)
    image = image.resize((1000, height))
    img = ImageTk.PhotoImage(image)

    text = 'Generated a graph'
    messagebox.showinfo(message = text)

    canvas.itemconfig(img_container, image = img)

    top.mainloop()

def mongo():
    if pkg_name:
        docdb.connect('mirror.cs.msu.ru', 'Sisyphus', 'noarch', 'special_db')
        if messagebox.askyesno(title = 'Update', message = 'Update database?'):
            docdb.update('mirror.cs.msu.ru', 'Sisyphus', 'noarch')
        result = docdb.show_pkg(pkg_name)
        text = 'Got packages'
        messagebox.showinfo(message = text)

        viewer(result)
    else:
        text = 'Enter package name first'
        messagebox.showerror(message = text)

def relative():
    if pkg_name:
        if messagebox.askyesno(title = 'Update', message = 'Update database?'):
            db_creator.update('mirror.cs.msu.ru', 'Sisyphus', 'x86%5f64')
        result = db_creator.show_pkg(pkg_name, 'packages_local.db')
        text = 'Got packages'
        messagebox.showinfo(message = text)

        viewer(result)
    else:
        text = 'Enter package name first'
        messagebox.showerror(message = text)

def redis():
    if pkg_name:
        red.connect()
        if messagebox.askyesno(title = 'Update', message = 'Update database?'):
            red.update('mirror.cs.msu.ru', 'Sisyphus', 'x86%5f64-i586')
        result = red.show_pkg(pkg_name)
        text = 'Got packages'
        messagebox.showinfo(message = text)

        viewer(result)
    else:
        text = 'Enter package name first'
        messagebox.showerror(message = text)

def create_button(name, func, master = None, width = 5, row = 0, column = 0):
    button = tkinter.Button(
            master = master,
            command = func,
            text = name,
            width = width,
            height = 1,
            bg = 'grey',
            fg = 'yellow'
            )
    button.grid(row = row, column = column)

global top
top = tkinter.Tk()
frame_1 = tkinter.Frame(master = top)
frame_2 = tkinter.Frame(master = top)
frame_3 = tkinter.Frame(master = top)
frame_4 = tkinter.Frame(master = top)

canvas = tkinter.Canvas(master = frame_4, width = 1000, height = 500)
canvas.pack()
img = tkinter.PhotoImage(file = 'penguin.png')
img_container = canvas.create_image(0, 0, anchor = 'nw', image = img)

entry = tkinter.Entry(master = frame_1, fg = 'black', bg = 'white', width = 50)
entry.grid(row = 0, column = 0)

create_button('Enter', get_text, frame_1, 5, 0, 1)
create_button('Search for package with sqlite', relative, frame_3, 25, 1)
create_button('Search for package with mongodb', mongo, frame_3, 25, 1, 1)
create_button('Search for package with redis', redis, frame_3, 25, 1, 2)
create_button('Exit', sys.exit, frame_3, 5, 2, 1)

label = tkinter.Label(master = frame_2, text = 'Package searcher')
label.pack()

frame_2.pack()
frame_1.pack()
frame_3.pack()
frame_4.pack()
top.mainloop()
