import sqlite3
import subprocess 
import argparse

def get_provs_from_db(package_name, database_name):
    answer = []
    con = sqlite3.connect(database_name)
    cur = con.cursor()
    for row in cur.execute("SELECT prov_list FROM requirements WHERE pkg_name = '%s'" % package_name):
        answer += ''.join(row).split(',')
    con.close()
    
    return answer

def get_reqs_from_db(package_name, database_name):
    answer = []
    con = sqlite3.connect(database_name)
    cur = con.cursor()
    for row in cur.execute("SELECT dep_list FROM requirements WHERE pkg_name = '%s'" % package_name):
        answer += ''.join(row).split(',')
    con.close()

    return answer

def _get_pkg_deps(source, branch, arch):
    deps = subprocess.run(('zsh', 'deps.sh', source, branch, arch), capture_output = True)
    pkg = {}
    for package in deps.stdout.decode().split('\n'):
        try:
            if package.split()[0] in pkg:
                pkg[package.split()[0]] += ',%s' % package.split()[1]
            else:
                pkg[package.split()[0]] = '%s' % package.split()[1]
        except:
            pass
    return pkg

def _get_pkg_provs(source, branch, arch):
    deps = subprocess.run(('zsh', 'prov.sh', source, branch, arch), capture_output = True)
    pkg = {}
    for package in deps.stdout.decode().split('\n'):
        try:
            if package.split()[0] in pkg:
                pkg[package.split()[0]] += ',%s' % package.split()[1]
            else:
                pkg[package.split()[0]] = '%s' % package.split()[1]
        except:
            pass
    return pkg

def delete_pkgs(name):
    try:
        cur = con.cursor()
        cur.execute('''DELETE FROM requirements WHERE pkg_name=%s''' % name)
        con.commit()
    except:
        return 'Error, no such package %s in a database' % name

def insert_pkgs(name, deps, prov):
    cur = con.cursor()
    cur.execute('''INSERT INTO requirements VALUES (?, ?, ?)''', (name, deps, prov))
    con.commit()

def update(source, branch, arch):
    dict1 = _get_pkg_deps(source, branch, arch)
    dict2 = _get_pkg_provs(source, branch, arch)
    set_1 = set(dict1.keys())
    set_2 = set(dict2.keys())
    set_intersection = set_1.intersection(set_2)

    for name in set_intersection:
        insert_pkgs(name, dict1[name], dict2[name])


def db_creator(source, branch, arch, db_name = 'package.db' ):
    global con
    con = sqlite3.connect(db_name)
    cur = con.cursor()
    try:
        cur.execute('''CREATE TABLE requirements (pkg_name text, dep_list text, prov_list text)''')
    except:
        pass
    update(source, branch, arch)
    con.close()

def show_pkg(pkg_name, db_name = 'package.db'):
    deps = get_reqs_from_db(pkg_name, db_name)
    provs = get_provs_from_db(pkg_name, db_name)

    return {'name': pkg_name, 'deps': deps, 'provs': provs}
    
def connect(db_name):
    global con
    con = sqlite3.connect(db_name)


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--source',
            help = 'provide source',
            required = True)
    arg_parser.add_argument('--branch',
            help = 'provide name of the branch',
            required = True)
    arg_parser.add_argument('--arch',
            help = 'provide arch',
            required = True)
    arg_parser.add_argument('--db_name',
            help = 'provide name of target data base file',
            default = 'package.db',
            required = False)
    args = arg_parser.parse_args()
    db_creator(**vars(args))
