from pymongo import MongoClient as _MC
import argparse as _arg
import subprocess as _sub


def _get_pkg_deps(source, branch, arch):
    deps = _sub.run(('zsh', 'deps.sh', source, branch, arch), capture_output = True)
    pkg = {}
    for package in deps.stdout.decode().split('\n'):
        try:
            if package.split()[0] in pkg:
                pkg[package.split()[0]] += package.split()[1:2]
            else:
                pkg[package.split()[0]] = package.split()[1:2]
        except:
            pass
    return pkg

def _get_pkg_provs(source, branch, arch):
    deps = _sub.run(('zsh', 'prov.sh', source, branch, arch), capture_output = True)
    pkg = {}
    for package in deps.stdout.decode().split('\n'):
        try:
            if package.split()[0] in pkg:
                pkg[package.split()[0]] += package.split()[1:2]
            else:
                pkg[package.split()[0]] = package.split()[1:2]
        except:
            pass
    return pkg


def insert_pkgs(name, deps, provs):
    try:
        return posts.insert_one({'name': name, 'deps': deps, 'provs': provs})
    except:
        pass

def delete_pkgs(pkg_name):
    try:
        posts.delete_one({'name': pkg_name})
    except:
        return 'Error: no such package %s in database' % pkg_name

def what_depends(pkg):
    try:
        return [p['name'] for p in posts.find({'deps': pkg})]
    except:
        pass

def depends(pkg):
    try:
        return posts.find_one({'name': pkg})['deps']
    except:
        pass

def what_provides(pkg):
    try:
        return posts.find_one({'provs': pkg})['name']
    except:
        pass

def provides(pkg):
    try:
        return posts.find_one({'name': pkg})['provs']
    except:
        pass

def show_pkg(pkg):
    return {'name': pkg, 'deps': depends(pkg), 'provs': provides(pkg)}

def update(source, branch, arch):
    dict1 = _get_pkg_deps(source, branch, arch)
    dict2 = _get_pkg_provs(source, branch, arch)
    set_1 = set(dict1.keys())
    set_2 = set(dict2.keys())

    for name in set_1.intersection(set_2):
        insert_pkgs(name, dict1[name], dict2[name])

def connect(source, branch, arch, db_name = 'package_database'):
    client = _MC()
    db = client.get_database(db_name)
    global posts
    posts = db.posts

if __name__ == '__main__':
    arg_parser = _arg.ArgumentParser()
    arg_parser.add_argument('--source',
            help = 'provide source',
            required = True)
    arg_parser.add_argument('--branch',
            help = 'provide name of the branch',
            required = True)
    arg_parser.add_argument('--arch',
            help = 'provide arch',
            required = True)
    arg_parser.add_argument('--db_name',
            help = 'provide name of target data base file',
            default = 'package_database',
            required = False)
    args = arg_parser.parse_args()
    connect(**vars(args))
