import redis
import subprocess as _sub
from redisgraph import Node, Edge, Graph, Path

def _get_pkg_deps(source, branch, arch):
    deps = _sub.run(('zsh', 'deps.sh', source, branch, arch), capture_output = True)
    pkg = {}
    for package in deps.stdout.decode().split('\n'):
        try:
            if package.split()[0] in pkg:
                pkg[package.split()[0]] += package.split()[1:2]
            else:
                pkg[package.split()[0]] = package.split()[1:2]
        except:
            pass
    return pkg

def _get_pkg_provs(source, branch, arch):
    deps = _sub.run(('zsh', 'prov.sh', source, branch, arch), capture_output = True)
    pkg = {}
    for package in deps.stdout.decode().split('\n'):
        try:
            if package.split()[0] in pkg:
                pkg[package.split()[0]] += package.split()[1:2]
            else:
                pkg[package.split()[0]] = package.split()[1:2]
        except:
            pass
    return pkg

def show_provides(pkg_name):
    params = {'name' : pkg_name}
    query = """MATCH (p:package {name: $name})-[r:provides]->(t:package)
                RETURN p.name, t.name"""
    answer = set()
    for obj in redis_graph.query(query, params).result_set:
        answer.add(obj[1])
    return list(answer)

def show_depends(pkg_name):
    params = {'name' : pkg_name}
    query = """MATCH (p:package {name: $name})-[r:depends]->(t:package)
                RETURN p.name, t.name"""
    answer = set()
    for obj in redis_graph.query(query, params).result_set:
        answer.add(obj[1])
    return list(answer)

def delete_node(pkg_name):
    params = {'name' : pkg_name}
    query = """MATCH (p:package { name: $name})
                DELETE p"""
    return redis_graph.query(query, params)

def show_pkg(pkg_name):
    return {'name': pkg_name, 'deps': show_depends(pkg_name), 'provs': show_provides(pkg_name)}

def insert_pkgs(name, deps, provs):
    pkg = Node(label = 'package', properties = {'name': name})
    redis_graph.add_node(pkg)

    for obj in deps:
        dep = Node(label = 'package', properties = {'name': obj})
        redis_graph.add_node(dep)
        edge = Edge(pkg, 'depends', dep)
        redis_graph.add_edge(edge)

    for obj in provs:
        prov = Node(label = 'package', properties = {'name': obj})
        redis_graph.add_node(prov)
        edge = Edge(pkg, 'provides', prov)
        redis_graph.add_edge(edge)

    redis_graph.commit()

def update(source, branch, arch):
    dict1 = _get_pkg_deps(source, branch, arch)
    dict2 = _get_pkg_provs(source, branch, arch)
    set_1 = set(dict1.keys())
    set_2 = set(dict2.keys())
    
    count = 0
    for name in set_1.intersection(set_2):
        print(name)
        insert_pkgs(name, dict1[name], dict2[name])
        count += 1
        if count == 100:
            break

def connect():
    r = redis.Redis(host = 'localhost', port = 6379)

    global redis_graph
    redis_graph = Graph('packages', r)
